package com.example.mvicoredemo.domain.todo

import com.badoo.mvicore.element.Actor
import com.badoo.mvicore.element.Bootstrapper
import com.badoo.mvicore.element.Reducer
import com.badoo.mvicore.feature.ActorReducerFeature
import com.example.mvicoredemo.domain.todo.model.Todo
import com.example.mvicoredemo.domain.todo.model.TodoState
import com.example.mvicoredemo.domain.todo.model.TodoWish
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodoFeature @Inject constructor(
    todoSource: TodoSource
) : ActorReducerFeature<TodoWish, TodoFeature.Effect, TodoState, Nothing>(
    initialState = TodoState(emptyList()),
    bootstrapper = BootStrapperImpl(),
    actor = ActorImpl(todoSource),
    reducer = ReducerImpl()
) {

    sealed class Effect {
        data class Loaded(val payload: List<Todo>) : Effect()
    }

    class BootStrapperImpl : Bootstrapper<TodoWish> {
        override fun invoke(): Observable<TodoWish> = Observable.just(TodoWish.Load)
    }

    class ActorImpl constructor(
        private val todoSource: TodoSource
    ) : Actor<TodoState, TodoWish, Effect> {

        override fun invoke(state: TodoState, action: TodoWish): Observable<Effect> =
            when (action) {
                is TodoWish.Add -> {
                    todoSource.addTodo(action.todo)
                        .andThen(todoSource.getTodos())
                }
                is TodoWish.Remove ->
                    todoSource.removeTodo(action.todo)
                        .andThen(todoSource.getTodos())
                TodoWish.Load ->
                    todoSource.getTodos()
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map<Effect> { Effect.Loaded(it) }
                .toObservable()
    }

    class ReducerImpl() : Reducer<TodoState, Effect> {
        override fun invoke(state: TodoState, effect: Effect): TodoState {
            return when (effect) {
                is Effect.Loaded -> state.copy(todos = effect.payload)
            }
        }
    }
}