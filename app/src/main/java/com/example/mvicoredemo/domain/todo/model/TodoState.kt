package com.example.mvicoredemo.domain.todo.model

data class TodoState(val todos: List<Todo>)