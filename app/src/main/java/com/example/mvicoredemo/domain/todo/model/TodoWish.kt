package com.example.mvicoredemo.domain.todo.model

sealed class TodoWish {
    object Load : TodoWish()
    data class Add(val todo: Todo) : TodoWish()
    data class Remove(val todo: Todo) : TodoWish()
}