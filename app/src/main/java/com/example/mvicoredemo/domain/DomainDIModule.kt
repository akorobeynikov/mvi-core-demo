package com.example.mvicoredemo.domain

import dagger.Module

@Module(includes = [DomainDIModule.Declarations::class])
object DomainDIModule {

    @Module
    internal interface Declarations {
        //TBD
    }
}