package com.example.mvicoredemo.domain.todo

import com.example.mvicoredemo.domain.todo.model.Todo
import io.reactivex.Completable
import io.reactivex.Single

interface TodoSource {
    fun getTodos(): Single<List<Todo>>
    fun addTodo(todo: Todo): Completable
    fun removeTodo(todo: Todo): Completable
}