package com.example.mvicoredemo.domain.todo.model

data class Todo(val id: Int, val text: String)