package com.example.mvicoredemo.presentation.todo

import com.example.mvicoredemo.domain.todo.model.Todo

sealed class TodoUiEvent {
    data class AddClicked(val text: String) : TodoUiEvent()
    data class RemoveClicked(val todo: Todo) : TodoUiEvent()
}