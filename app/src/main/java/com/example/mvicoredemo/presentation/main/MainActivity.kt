package com.example.mvicoredemo.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvicoredemo.databinding.ActivityMainBinding
import com.example.mvicoredemo.presentation.main.rv.TodoRvAdapter
import com.example.mvicoredemo.presentation.todo.TodoBindings
import com.example.mvicoredemo.presentation.todo.TodoModel
import com.example.mvicoredemo.presentation.todo.TodoUiEvent
import com.example.mvicoredemo.presentation.todo.TodoView
import dagger.android.AndroidInjection
import io.reactivex.Observer
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MainActivity : AppCompatActivity(), TodoView {

    private lateinit var viewBinding: ActivityMainBinding

    @Inject
    lateinit var featureBinder: TodoBindings

    private val eventSource = PublishSubject.create<TodoUiEvent>()

    private val rvAdapter: TodoRvAdapter = TodoRvAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        setupRv()
        setupListeners()
        featureBinder.setup(this)
    }

    override fun subscribe(observer: Observer<in TodoUiEvent>) {
        eventSource.subscribe(observer)
    }

    override fun accept(model: TodoModel?) {
        model?.let { render(it) }
    }

    private fun setupRv() {
        viewBinding.todoRv.apply {
            adapter = rvAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun setupListeners() {
        viewBinding.addButton.setOnClickListener {
            val text = viewBinding.textField.text.toString()
            eventSource.onNext(TodoUiEvent.AddClicked(text))
        }
        rvAdapter.onRemoveClickListener = { todo ->
            eventSource.onNext(TodoUiEvent.RemoveClicked(todo))
        }
    }

    private fun render(model: TodoModel) {
        rvAdapter.data = model.todos
    }
}
