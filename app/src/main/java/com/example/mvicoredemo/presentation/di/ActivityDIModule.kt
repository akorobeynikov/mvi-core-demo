package com.example.mvicoredemo.presentation.di

import com.example.mvicoredemo.presentation.main.MainActivity
import com.example.mvicoredemo.presentation.main.MainDiModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityDIModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainDiModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}