package com.example.mvicoredemo.presentation.main

import com.example.mvicoredemo.domain.todo.TodoFeature
import com.example.mvicoredemo.presentation.di.ActivityScope
import com.example.mvicoredemo.presentation.todo.TodoBindings
import dagger.Module
import dagger.Provides

@Module
object MainDiModule {
    @Provides
    @JvmStatic
    @ActivityScope
    fun provideBindings(
        activity: MainActivity,
        feature: TodoFeature
    ): TodoBindings = TodoBindings(activity, feature)
}