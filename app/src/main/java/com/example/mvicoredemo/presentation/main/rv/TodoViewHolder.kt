package com.example.mvicoredemo.presentation.main.rv

import androidx.recyclerview.widget.RecyclerView
import com.example.mvicoredemo.databinding.ItemTodoBinding

class TodoViewHolder(val binding: ItemTodoBinding) : RecyclerView.ViewHolder(binding.root)