package com.example.mvicoredemo.presentation.di

import javax.inject.Scope

@Scope
@MustBeDocumented
annotation class ActivityScope