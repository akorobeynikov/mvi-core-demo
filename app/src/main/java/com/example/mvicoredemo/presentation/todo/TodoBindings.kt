package com.example.mvicoredemo.presentation.todo

import com.badoo.mvicore.android.AndroidBindings
import com.badoo.mvicore.binder.using
import com.example.mvicoredemo.domain.todo.TodoFeature
import com.example.mvicoredemo.domain.todo.model.Todo
import com.example.mvicoredemo.domain.todo.model.TodoState
import com.example.mvicoredemo.domain.todo.model.TodoWish
import com.example.mvicoredemo.presentation.main.MainActivity

class TodoBindings constructor(
    view: MainActivity,
    private val todoFeature: TodoFeature
) : AndroidBindings<TodoView>(view) {
    override fun setup(view: TodoView) {
        binder.bind(view to todoFeature using TodoUiEventMapper())
        binder.bind(todoFeature to view using TodoStateMapper())
    }
}

class TodoUiEventMapper : (TodoUiEvent) -> TodoWish? {
    override fun invoke(event: TodoUiEvent): TodoWish? = when (event) {
        is TodoUiEvent.AddClicked -> TodoWish.Add(Todo(0, event.text))
        is TodoUiEvent.RemoveClicked -> TodoWish.Remove(event.todo)
    }
}

class TodoStateMapper : (TodoState) -> TodoModel {
    override fun invoke(state: TodoState): TodoModel = TodoModel(state.todos)
}
