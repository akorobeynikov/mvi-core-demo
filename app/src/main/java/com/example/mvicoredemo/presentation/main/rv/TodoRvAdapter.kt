package com.example.mvicoredemo.presentation.main.rv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvicoredemo.databinding.ItemTodoBinding
import com.example.mvicoredemo.domain.todo.model.Todo

class TodoRvAdapter : RecyclerView.Adapter<TodoViewHolder>() {
    var data: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onRemoveClickListener: ((Todo) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding = ItemTodoBinding.inflate(LayoutInflater.from(parent.context))
        return TodoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.binding.textView.text = data[position].text
        holder.binding.removeButton.setOnClickListener {
            onRemoveClickListener?.invoke(data[position])
        }
    }

    override fun getItemCount() = data.size
}
