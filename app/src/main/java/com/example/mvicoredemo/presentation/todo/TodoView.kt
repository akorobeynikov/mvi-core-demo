package com.example.mvicoredemo.presentation.todo

import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer

interface TodoView : ObservableSource<TodoUiEvent>, Consumer<TodoModel>