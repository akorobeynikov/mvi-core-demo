package com.example.mvicoredemo.presentation.todo

import com.example.mvicoredemo.domain.todo.model.Todo

data class TodoModel(val todos: List<Todo>)