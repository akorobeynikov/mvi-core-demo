package com.example.mvicoredemo

import android.app.Application
import com.example.mvicoredemo.data.DataDIModule
import com.example.mvicoredemo.domain.DomainDIModule
import com.example.mvicoredemo.presentation.di.PresentationDIModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        DataDIModule::class,
        DomainDIModule::class,
        PresentationDIModule::class,
        AppDIModule::class
    ]
)

@Singleton
interface AppDIComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppDIComponent
    }

    fun inject(app: App)
}