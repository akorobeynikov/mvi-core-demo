package com.example.mvicoredemo.data.todo

import com.example.mvicoredemo.data.db.TodoDatabase
import com.example.mvicoredemo.domain.todo.TodoSource
import com.example.mvicoredemo.domain.todo.model.Todo
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodoSourceImpl @Inject constructor(
    private val db: TodoDatabase
) : TodoSource {
    override fun getTodos(): Single<List<Todo>> {
        return db.todoDao().getAll()
            .map { it.map { entity -> TodoMapper.mapTodoEntity(entity) } }
    }

    override fun addTodo(todo: Todo): Completable {
        return db.todoDao().insert(TodoMapper.mapTodo(todo))
    }

    override fun removeTodo(todo: Todo): Completable {
        return db.todoDao().delete(TodoMapper.mapTodo(todo))
    }
}