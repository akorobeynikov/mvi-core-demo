package com.example.mvicoredemo.data

import android.content.Context
import androidx.room.Room
import com.example.mvicoredemo.data.db.TodoDatabase
import com.example.mvicoredemo.data.todo.TodoSourceImpl
import com.example.mvicoredemo.domain.todo.TodoSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [DataDIModule.Declarations::class])
object DataDIModule {

    @Provides
    @Singleton
    @JvmStatic
    fun provideDatabase(context: Context): TodoDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            TodoDatabase::class.java, "todo-database"
        ).build()
    }

    @Module
    internal interface Declarations {
        @Binds
        fun bindTodoSource(impl: TodoSourceImpl): TodoSource
    }
}