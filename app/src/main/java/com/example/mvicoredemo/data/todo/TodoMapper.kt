package com.example.mvicoredemo.data.todo

import com.example.mvicoredemo.data.db.TodoEntity
import com.example.mvicoredemo.domain.todo.model.Todo

internal object TodoMapper {
    fun mapTodo(todo: Todo): TodoEntity {
        return TodoEntity(todo.id, todo.text)
    }

    fun mapTodoEntity(todo: TodoEntity): Todo {
        return Todo(todo.uid, todo.text)
    }
}